<?php
class ControllerStats extends Controller{
    function __construct($conf, $var, $acc) {
        parent::__construct($conf, $var, $acc); 
    }
    public function main() {
        $success=TRUE;
        $code=200;
        $error=FALSE;
        
        $dat = (object) indexModel::bd($this->conf)->getDominio("totales")[0];
        $token=FALSE;        
        $array=array(
            "success"=>$success,
            "count_mutations"=>$dat->count_mutations,
            "count_no_mutation"=>$dat->count_no_mutation,
            "ratio"=>$dat->ratio
        );
        http_response_code($code);
        indexModel::bd($this->conf)->setJsonV1($error,$array);
    }

   
}   
?>