<?php
require_once __DIR__ . '/../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
class ControllerMutation extends Controller{
    function __construct($conf, $var, $acc) {
        parent::__construct($conf, $var, $acc); 
    }
    public function main() {
        $error=FALSE;
        $success=TRUE;
        $code=200;
        $rawdata = file_get_contents("php://input");
        $decoded = json_decode($rawdata,true);
        $result=$this->hasMutation($decoded["dna"]);
        if(!$result){
            $adn =$this->have_oblique($decoded["dna"]);
            $result=$this->hasMutation($adn);
        }
        $decoded["result"]=$result;
        $nc = json_encode($decoded,true);
        if(!$result){
            $code=403;
            $error=TRUE;
            $success=FALSE;
        }
        $connection = new AMQPStreamConnection('localhost', 5672, $this->conf['userrabbit'], $this->conf['claverabbit']);
        $channel = $connection->channel();
        $channel->queue_declare('rama', false, true, false, false);
        $msg = new AMQPMessage($nc);
        $channel->basic_publish($msg, '', 'rama');
         /*
         $callback = function ($msg) {
            return  $msg->body;
          };
          $channel->basic_consume('rama', '', false, true, false, false, $callback);
          $channel->wait();
          
        */
        $channel->close();
        $connection->close();
        $array=array("Servicio-API"=>"ADN","mensaje"=>indexModel::bd($this->conf)->getMensajeApi(""),"result"=>$result);
        $token=FALSE;        
        $array=array(
            "success"=>$success,
            "data"=>$array,
        );
        http_response_code($code);
        indexModel::bd($this->conf)->setJsonV1($error,$array);
    }

    private function hasMutation($adn){
        $res=false;
        $cad = array("AAAA","TTTT","GGGG","CCCC");
        foreach ($cad as $k => $v){
            if($this->array_find($v, $adn)){
                $res = true;
                break;
            }
        }
        return $res;
    }

    private function array_find($abuscar,$adn){
        $res = false;
        foreach ($adn as $item)
        {
            if (strpos($item, $abuscar) !== FALSE)
            {
                $res = true;
                break;
            }
        }
        return $res;
    }

    private function have_oblique($adn){
        $narray=null;
        $nr = strlen($adn[0]);
        foreach($adn as $k => $v){
            for($i=0;$i<$nr;$i++){
                $arr1 = str_split($v);
                $cadnue[$i]= $cadnue[$i].$arr1[$i];
            }
        }
        return  $cadnue;
    }
}   
?>