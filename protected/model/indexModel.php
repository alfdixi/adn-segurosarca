<?php
class indexModel {

    public $db;
    private $host;
    private $bd;
    private $user;
    private $clave;
    public $pathSite;
    private $conf;
    private $estructura;
    private static $tituloAlternox;

    function __construct($conf) {
        //Traemos la unica instancia de PDO
        require_once $conf['folderModelos'] . 'SPDO.php';
        $host = $conf['host'];
        $bd = $conf['dbname'];
        $this->bd = $conf['dbname'];
        $user = $conf['username'];
        $clave = $conf['password'];
        $this->conf = $conf;
        $this->pathSite = $conf['pathSite'];
        $this->db = SPDO::singleton($host, $bd, $user, $clave);
    }
    
    public function getMensajeApi($id=""){
        $mensajes=array(
            ""=>"API - ADN Seguros Arca.",
            "errorUser"=>"ERR-1001: Cliente ID o  Cliente Secret incorrectos",
            "successGetEstructura"=>"SUCCESS.",
            "errorToken"=>"ERR-1001:El token no es valido",
            "expirateToken"=>"ERR-1001:El token ya vencio",
            "successRegistrodeUsuarios"=>"",
        );
        $cad = $mensajes[$id];
        return $cad;
    }
    
    public function setJsonV1($error,$array){
        $data = array(
            "error"=>$error,
            "version"=>"1",
            "response"=>$array
        );
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    public function getHascamposAll($table, $id = null) {
        return Catalogos::getRelacionTable($this->db, $this->bd, $table, $id);
    }

    public function getEstructuraTable($table) {
        return Catalogos::getStructureTable($this->bd,$this->db, $table);
    }

    public static function getNameDominio($var) {
        $dat = explode("/", $var["con"]);
        return $dat[1];
    }

    public static function bd($config) {
        return new indexModel($config);
    }

    public function getSQL($sql) {
        return Catalogos::getSql($this->bd,$this->db, $sql);
    }

    public function getDominio($table, $id = null, $limit = null) {
        return Catalogos::getData($this->db, $table, $this->bd, $id, $limit);
    }

    public function getDominioID($table, $valores = null) {
        return Catalogos::getDataArray($this->db, $table, $this->bd, $valores);
    }

    public function getIDField($table, $campo, $valor) {
        return Catalogos::getDataForField($this->db, $table, $campo, $valor);
    }

    public function getcampos($table) {
        return Catalogos::getFields($this->bd,$this->db, $table);
    }

    public function getcamposAll($table) {
        return Catalogos::getFieldsAll($this->db, $table, $this->bd);
    }

    public function updateDominio($datos, $id = null) {
        //var_dump($datos);
        $camposRelacionados = null;
        foreach ($datos as $key => $value) {
            if (substr($key, 0, 4) == "Xrel") {
                $camposRelacionados[substr($key, 4)] = $value;
            }
            if (substr($key, 0, 3) == "txt") {
                $campos[substr($key, 3)] = $value;
            }
        }
        if ($id == 0 || $id == "") {
            //echo "INSERT";
            $cad = Catalogos::guardarRegistro($this->conf, $this->bd, $this->pathSite, $this->db, $datos["Dominio"], $campos, $camposRelacionados);
        } else {
            //echo "UPDATE";
            $cad = Catalogos::editarRegistro($this->conf, $this->bd, $this->pathSite, $this->db, $datos["Dominio"], $campos, $camposRelacionados, $id);
        }
        return $cad;
    }

    public function deleteDominio($table, $id) {
        return Catalogos::borrarRegistro($this->conf, $this->db, $table, $id);
    }

    public function getMensaje($data) {
        $color = "danger";
        $colorx = "red";
        if ($data["isCorrect"]) {
            $color = "success";
            $colorx = "green";
        }

        $campos = "";
        if(isset($data["txt"])){
          foreach ($data["txt"] as $key => $value) {
              if ($key != "con") {
                  $campos.='<input type="hidden" name="' . $key . '" value="' . $value . '">'.PHP_EOL ;
              }
          }
        }

        $res = '
        <form action="' . $data["return"] . '" method="post" name="fmReturn" id="fmReturn">
          ' . $campos . '
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <div class="content-box border-top border-' . $colorx . '">
                                <h3 class="content-box-header clearfix">
                                    ' . $data["tituloMensaje"] . '
                                    <small></small>

                                </h3>
                                <div class="content-box-wrapper">
                                    <p><div class="alert alert-' . $color . '">' . $data["Mensaje"] . '</div></p>
                                    <div class="divider"></div>
                                    <div class="loading-spinner">
                                        <i class="bg-' . $colorx . '"></i>
                                        <i class="bg-' . $colorx . '"></i>
                                        <i class="bg-' . $colorx . '"></i>
                                        <i class="bg-' . $colorx . '"></i>
                                        <i class="bg-' . $colorx . '"></i>
                                        <i class="bg-' . $colorx . '"></i>
                                    </div>
                                </div>
                            </div>




        <!--
        <div class="alert alert-' . $color . '">
            ' . $campos . '
            <strong>' . $data["tituloMensaje"] . ' </strong>
            ' . $data["Mensaje"] . '
            </div>
            -->
            </div>
            </form>
                <script>

                    function iraFormulario(){
                        document.getElementById("fmReturn").submit();
                    }
                    setTimeout(function(){ iraFormulario(); }, ' . $data["tiempo"] . '000);

                </script>';
        return $res;
    }

    public function SecurityParams($_Cadena) {
        $_Cadena = htmlspecialchars(trim(addslashes(stripslashes(strip_tags($_Cadena)))));
        $_Cadena = str_replace(chr(160),'',$_Cadena);
        return $_Cadena;
        //return mysql_real_escape_string($_Cadena);
    }
}
?>
