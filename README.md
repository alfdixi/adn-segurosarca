# ADn-segurosarca
## _Desafío Sr. Full Stack Developer_

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Requerimos que desarrolles un proyecto que detecte si una persona tiene diferencias genéticas basándose en su secuencia de ADN. Para eso es necesario crear un programa con un método o función con la siguiente firma: boolean hasMutation(String[] dna);


- Instalación
- Configuración
- ✨ Pruebas finales ✨

## Instalación

En este archivo se debera configurar las rutas y accesos a la base de datos

El proyecto va dentro de la carpeta principal del servidor de aplicaciones web  puede ser Apache o Nginx.
En caso de ser Nginx debera de migrarse el archivo .htaccess para que opere en Nginx.

> Si el sistema debiéra operar con demaciado tráfico recomendaria Nginx
> de lo contrario con un Apache es suficiente

habra que instalar la libreria de RabbitMQ con composer 
https://www.rabbitmq.com/tutorials/tutorial-one-php.html

## Configuración

- Configurar el archivo ./protected/config/data.php

Los datos que se configuran son los datos de la conexion ala base de datos y las rutas donde opera el desarrollo.

## Instalación extra

Aparte de Msyql, PHP habra que instalar el Broker de RabbitMQ instalado para manejar grandes cantidades de transaccioens, dependiendo donde se realise la instalación pueden consulatr la documentación oficial 

https://www.rabbitmq.com/download.html

tambien pueden seguir este manual 
https://computingforgeeks.com/how-to-install-latest-rabbitmq-server-on-ubuntu-linux/


## Configuración

- Para RabbiMQ deberan de generar un Exchanges llamado **adn**
- Habra que generar un Queues llamado **rama**

y dejar los parametros por default 
- usuario:guest
- clave: guest
- puerto: 15672


## Para las pruebas

```sh
http://phpmexico.com/adn-segurosarca/
```

El sistema opera con dos servicios 

** SERVICIO mutation **
```sh
http://phpmexico.com/adn-segurosarca/mutation
```

Mutation
Este servicio recibe un array en formatoJSON que simulaser una cadena de ADN a la cual se le validaran recurencias de mutaciones  


```sh
endpoint: get   /apiadn/mutation
```

** SERVICIO mutation **
```sh
http://phpmexico.com/adn-segurosarca/stats
```

este servicio  devuelva un JSON con las estadísticas de las verificaciones de ADN

```sh
endpoint: POST   /apiadn/stats
```

-- Response --

```sh
{
"count_mutations": 40,
"count_no_mutation": 100,
"ratio": 0.4
}
```




## License

MIT

**JACS!**

