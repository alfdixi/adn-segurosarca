<?php
/**
 * ••• ADN SegurosArca ••• 
 * » Sistema de prueba
 * @package Principal
 * @subpackage admin
 * @author Castillejos Sánchez José Alfredo <alfdixi@gmail.com>
 * @copyright Copyright (c) 2021,
 * @link http://api.phpmexico.com
 * @category Class Access
 * @version 0.1 2021-05-04 11:01:00
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since v 0.1 2021-05-04 11:01:00
 * @access public
 */
ini_set('display_errors',0);
error_reporting(E_ALL); 
$dixi=dirname(__FILE__).'/framework/DIXI.php';
/**
 * ••• Descripción •••
 * » Link de Acceso a los datos de configuración
 * @var string Link de Acceso a los datos de configuración
 * @name $config
 * @access private
 * @see dixi
 */
$config=dirname(__FILE__).'/protected/config/data.php';
// --> LLama el archivo del framework
require_once($dixi);
// --> Creación de la aplicación Web
DIXI::crearAplicacionWeb($config)->run();
?>