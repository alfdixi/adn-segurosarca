<?php
ini_set('display_errors',0);
error_reporting(E_ALL);
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
// --> Conection RAbbitMQ
$config=dirname(__FILE__).'/protected/config/data.php';
$conf = require($config);
$connection = new AMQPStreamConnection('localhost', 5672, $conf['userrabbit'], $conf['claverabbit']);
$channel = $connection->channel();
$callback = function ($msg) {
    $config=dirname(__FILE__).'/protected/config/data.php';
    $conf = require($config);
    require_once dirname(__FILE__).'/'.$conf['folderModelos'].'SPDO.php';
    $db = SPDO::singleton($conf['host'],$conf['dbname'],$conf['username'],$conf['password']);

    $d = json_decode($msg->body,true);
    //var_dump($d);
    $mutado = 2;
    if($d["result"]){
        $mutado = 1;
    }
    $sqlValidate  ="INSERT INTO adn VALUES (0,'$msg->body',NOW(),$mutado)";
    $recordset = $db->prepare($sqlValidate);
    $recordset->execute();
    // --> Sacar totales 
    $sqlValidate  ="SELECT COUNT(*) AS nr FROM  adn WHERE mutado_id=1";
    $recordset = $db->prepare($sqlValidate);
    $recordset->execute();
    $item1 = $recordset->fetch(PDO::FETCH_OBJ);
    $count_mutations = $item1->nr;
    $sqlValidate  ="SELECT COUNT(*) AS nr FROM  adn WHERE mutado_id=2";
    $recordset = $db->prepare($sqlValidate);
    $recordset->execute();
    $item2 = $recordset->fetch(PDO::FETCH_OBJ);
    $count_no_mutation = $item2->nr;
    $st = $count_mutations  + $count_no_mutation ;
    $ratio = ($st / $count_mutations ) * 100;
    $sqlValidate  ="UPDATE totales SET count_mutations = $count_mutations, count_no_mutation =$count_no_mutation,ratio = $ratio WHERE id=1";
    $recordset = $db->prepare($sqlValidate);
    $recordset->execute();
};

$channel->basic_consume('rama', '', false, true, false, false, $callback);
while ($channel->is_consuming()) {
    $channel->wait();
}
?>